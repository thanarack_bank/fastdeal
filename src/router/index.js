import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import NewStoreComponent from '../components/NewStoreComponent'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello
    },
    {
      path: '/new-store',
      name: 'newStoreComponent',
      component: NewStoreComponent
    }
  ]
})
